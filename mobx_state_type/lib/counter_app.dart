import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:modx_state_type/model/counter_model.dart';

class CounterApp extends StatelessWidget {
  final String title;

  CounterApp(this.title);

  final Counter counter = Counter();

  @override
  Widget build(BuildContext context) {
    print('rebuilds APP');

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have pushed the button this many times:'),
            Observer(
                builder: (_) => Text(
                      '${counter.value}',
                      style: const TextStyle(fontSize: 40),
                    )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: counter.increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
