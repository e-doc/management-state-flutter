import 'package:mobx/mobx.dart';

part 'counter_model.g.dart';

class Counter = _Counter with _$Counter;

abstract class _Counter with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}

// run command flutter packages pub run build_runner build
