import 'package:flutter/material.dart';
import 'package:multiple_scoped_model_type/model/counter_model.dart';
import 'package:multiple_scoped_model_type/model/user_model.dart';
import 'package:scoped_model/scoped_model.dart';

class CounterApp extends StatelessWidget {
  final String title;

  CounterApp(this.title);

  @override
  Widget build(BuildContext context) {
    print('rebuild APP');

    return ScopedModel<CounterModel>(
      model: CounterModel(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('You have pushed the button this many times:'),
              ScopedModelDescendant<UserModel>(
                builder: (context, child, model) {
                  print('rebuild Widget descendent UserModel');
                  return Text(
                    model.userName.toString(),
                    style: Theme.of(context).textTheme.display1,
                  );
                },
              ),
              ScopedModelDescendant<CounterModel>(
                builder: (context, child, model) {
                  print('rebuild Widget descendent Counter Model');
                  return Text(
                    model.counter.toString(),
                    style: Theme.of(context).textTheme.display1,
                  );
                },
              ),
            ],
          ),
        ),
        // Use the ScopedModelDescendant again in order to use the increment
        // method from the CounterModel

        floatingActionButton: ScopedModelDescendant<CounterModel>(
          builder: (context, child, model) {
            return FloatingActionButton(
              onPressed: model.increment,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            );
          },
        ),
      ),
    );
  }
}
