import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class UserModel extends Model {
  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);

  String _userName = 'Seu Joao da Silva';

  String get userName => _userName;

  void increment() {
    _userName = 'juca';

    notifyListeners();
  }

  void setUsername(String username) {
    _userName = username;

    notifyListeners();
  }
}
