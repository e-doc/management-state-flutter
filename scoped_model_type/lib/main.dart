import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_type/model/abstract_model.dart';

import 'counter_app.dart';
import 'model/counter_model.dart';

void main() {
  runApp(MyApp(
    model: CounterModel(),
  ));
}

class MyApp extends StatelessWidget {
  final AbstractModel model;

  const MyApp({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // At the top level of our app, we'll, create a ScopedModel Widget. This
    // will provide the CounterModel to all children in the app that request it
    // using a ScopedModelDescendant.
    return ScopedModel<AbstractModel>(
      model: model,
      child: MaterialApp(
        title: 'Scoped Model Demo',
        home: CounterApp('Scoped Model Demo'),
      ),
    );
  }
}
