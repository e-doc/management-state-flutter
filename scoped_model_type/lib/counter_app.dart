import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_type/model/abstract_model.dart';

class CounterApp extends StatelessWidget {
  final String title;

  CounterApp(this.title);

  @override
  Widget build(BuildContext context) {
    print('rebuild APP');

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have pushed the button this many times:'),

            ScopedModelDescendant<AbstractModel>(
              builder: (context, child, model) {
                print('rebuild Widget descendent');
                return Text(
                  model.counter.toString(),
                  style: Theme.of(context).textTheme.display1,
                );
              },
            ),
            // Create a ScopedModelDescendant. This widget will get the
            // CounterModel from the nearest parent ScopedModel<CounterModel>.
            // It will hand that CounterModel to our builder method, and
            // rebuild any time the CounterModel changes (i.e. after we
            // `notifyListeners` in the Model).

            // ScopedModelDescendant<CounterModel>(
            //   builder: (context, child, model) {
            //     print('rebuild widget');
            //     return Text(
            //       model.counter.toString(),
            //       style: Theme.of(context).textTheme.display1,
            //     );
            //   },
            // ),
          ],
        ),
      ),
      // Use the ScopedModelDescendant again in order to use the increment
      // method from the CounterModel
      floatingActionButton: ScopedModelDescendant<AbstractModel>(
        builder: (context, child, model) {
          return FloatingActionButton(
            onPressed: model.increment,
            tooltip: 'Increment',
            child: Icon(Icons.add),
          );
        },
      ),
    );
  }
}
