import 'package:scoped_model/scoped_model.dart';

abstract class AbstractModel extends Model {
  int get counter;
  void increment();
}
