import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_type/model/abstract_model.dart';

class CounterModel extends AbstractModel {
  static CounterModel of(BuildContext context) =>
      ScopedModel.of<CounterModel>(context);

  int _counter = 0;

  int get counter => _counter;

  @override
  void increment() {
    _counter++;

    notifyListeners();
  }
}
